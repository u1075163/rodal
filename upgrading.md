If you want to upgrade rodal to work on a new version of rust:

1. Get the rust standard library source code for the rust-version you care about (e.g. by `rustup run <TOOLCHAIN> rustup component add rust-src`, the moma's should allready have this installed).
2. Browse to the source code (usually `<RUSTUP_HOME>\toolchains\<TOOLCHAIN>\lib\rustlib\src\rust\src`, where `<RUSTUP_HOME>` is typically `~/.rustup`, it is `/opt/rust` on the moma's).
3. Check that `src/rust_std.rs` is up to date with the rust standard library:
    1. You can probably safetly ignore any `rodal_object/object_reference/value/pointer/named!` macros, provided that references havn't changed sizes (if they do, you ought to get a compilation error).
    2. Check that each `struct` (or other non-trivial type) has *exactly* the same definition in the standard library (this may require you to create copies of other types if they are private), after making changes you will likley need to change any corresponding `rodal` macro invocations.
    3. Check that any manual implementions of dump methods are correct (and any assumptions mentioned in the comments still hold true).
    4. Check that any utility functions referenced by the above (such as RawTable::capacity) are still correct
4. If you modified anything above, you may need to update `extended_std.rs` accordingly, otherwise there's no need to look at it
5. Check that struct and enum fields havn't changed order, and if so update the `rodal_struct/enum` macro invocations, or manual dump implementation to dump them in the correct order. There are multiple ways to check (in ordeer from easiest to hardest). (Note: this applies to *any* types you use rodal to dump, not just the ones in the rodal crate):
    1. Use the `rodal_unordered_struct/enum` macros. This will however incur a *runtime* cost, but it may be the only option if Rust starts reordering fields of generic types based on the type paramaters (so far it dosn't seem to do this).
    2. Try and dump instances of each type (Zebu will dump most types), and if you get a 'cant move backwards' assertion failure, move the field it failed (you will need to compile rodal in debug mode). 
    You will get traces of the form `TRACE - level: +offset:  Type::function`, the broken dump function will be the `Type::function` of the trace with level 'l-1' (where 'l' is the level of the bottomost trace). The broken field will be the 'n+1'th field of that object, where 'n' is the number of traces of level 'l' that appear bellow the broken-dump function's record. Just dump the field in the correct order (the error message will tell you two pointers, their difference is the offset relative to the end of the 'n'th field). If you do dump a type, and do *not* get an error, it is definently dumping the fields in the correct order.
    3. Compile your code by passing `-Z print-type-sizes` to rustc, you will get a list of types with fields in the appropriate order. This however only works on *nightly* rust compilers.
6. Modify `build.rs` to check for your new rustc's version commit-hash (run `rustc -Vv` to get it).
7. Make sure rodal compiles (some of the mem transmutes may fail if types have changed size).
8. Make sure rodal can still dump stuff (e.g. try compiling a reasonably sized program with Zebu)

If your really lazy, just run the last 3 steps, and if everything went well, there's almost no chance you'll need to do any of the other steps.
