// Copyright 2017 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate rustc_version;

/// Keeps the version info for `rustc` versions supported by rodal.
/// Using the `rustc_version` crate, the current `rustc` version is examined
const SUPPORTED_VERSION: [(&'static str, &'static str); 2] = [
    (
        "1433507eba7d1a114e4c6f27ae0e1a74f60f20de",
        "1.30.1 (1433507eb 2018-11-07)",
    ),
    (
        "f47ec2ad5b6887b3d400aee49e2294bd27733d18",
        "1.34.0-nightly (f47ec2ad5 2019-02-14)",
    ),
];

fn main() {
    let cur_ver = rustc_version::version_meta().unwrap().commit_hash.unwrap();
    println!("Current version: {}", cur_ver);
    //    if SUPPORTED_VERSION
    //        .iter()
    //        .find(|version| version.0 == cur_ver)
    //        .is_none()
    //    {
    //        println!(
    //            "Supported versions: {}",
    //            SUPPORTED_VERSION
    //                .iter()
    //                .map(|x| x.1)
    //                .collect::<Vec<&'static str>>()
    //                .join(", ")
    //        );
    //        panic!("Unsupported Rust version");
    //    }
}
