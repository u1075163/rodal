extern crate rodal;
#[macro_use]
extern crate log;
extern crate stderrlog;
use rodal::AsmDumper;
use std::fs::File;

macro_rules! test_dump {
    ($val: expr, $ty: ty) => {
        let source: $ty = $val;
        AsmDumper::new(File::create("test.S").unwrap())
            .dump("dump", &source)
            .finish();
    };
}

fn enable_log() {
    //    stderrlog::new().verbosity(5).init().unwrap();
    match stderrlog::new().verbosity(5).init() {
        Ok(_) => info!("stderrlog initialized"),
        Err(E) => warn!("{}", E),
    }
}

#[test]
fn test_dump_vec() {
    test_dump!(vec![0; 5], Vec<u64>);
}

#[test]
fn test_dump_string() {
    test_dump!("helloworld".to_string(), String);
}

#[test]
fn test_dump_arc() {
    use std::sync::Arc;
    test_dump!(Arc::new(1), Arc<u64>);
}

#[test]
fn test_dump_rwlock() {
    use std::sync::RwLock;
    test_dump!(RwLock::new(1), RwLock<u64>);
}

#[test]
fn test_dump_mutex() {
    use std::sync::Mutex;
    test_dump!(Mutex::new(1), Mutex<u64>);
}

#[test]
fn test_dump_empty_hashmap() {
    use std::collections::HashMap;
    enable_log();

    let map: HashMap<u64, u64> = HashMap::new();
    test_dump!(map, HashMap<u64, u64>);
}

#[test]
fn test_dump_hashmap() {
    use std::collections::HashMap;

    let map = {
        let mut ret: HashMap<u64, u64> = HashMap::new();
        ret.insert(1, 1);
        ret.insert(2, 2);
        ret
    };

    test_dump!(map, HashMap<u64, u64>);
}

#[test]
fn test_dump_empty_linkedlist() {
    use std::collections::LinkedList;
    enable_log();

    test_dump!(LinkedList::new(), LinkedList<u64>);
}

#[test]
fn test_dump_linkedlist() {
    use std::collections::LinkedList;
    enable_log();

    let list = {
        let mut list: LinkedList<u64> = LinkedList::new();
        list.push_back(1);
        list.push_back(2);
        list
    };
    test_dump!(list, LinkedList<u64>);
}

#[test]
fn test_dump_option() {
    use std::option::Option;

    test_dump!(None, Option<u64>);
    test_dump!(Some(1), Option<u64>);
}

#[test]
fn test_dump_option_box() {
    use std::boxed::Box;
    use std::option::Option;

    test_dump!(None, Option<Box<u64>>);
    test_dump!(Some(Box::new(1)), Option<Box<u64>>);
}
