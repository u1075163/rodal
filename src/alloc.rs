// Copyright 2017 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
use super::*;

/// Checks whether an object is a rodal-bound or not,
///
/// * `ptr`: is a pointer to the target object
///
/// returns `true` if `ptr` points to a rodal-bound object
pub fn is_rodal_dump<T>(ptr: *const T) -> bool {
    let ptr = Address::from_ptr(ptr);
    match unsafe { RODAL_BOUND } {
        Some((start, end)) => start <= ptr && ptr < end,
        _ => false,
    }
}

use std::alloc::{GlobalAlloc, Layout, System};

pub struct RodalAlloc {
    pub sys: std::cell::UnsafeCell<System>,
}

impl RodalAlloc {
    fn get_sys(&self) -> &mut System {
        unsafe { std::mem::transmute(self.sys.get()) }
    }
}
unsafe impl Sync for RodalAlloc {}
unsafe impl Send for RodalAlloc {}

/// Implements the trait methods required to make `RodalAlloc` the global allocator
/// - Trait methods here are wrappers around the trait methods of `ref RodalAlloc`
unsafe impl GlobalAlloc for RodalAlloc {
    #[inline]
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        (&self).alloc(layout)
    }
    #[inline]
    unsafe fn alloc_zeroed(&self, layout: Layout) -> *mut u8 {
        (&self).alloc_zeroed(layout)
    }
    #[inline]
    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        (&self).dealloc(ptr, layout)
    }
    #[inline]
    unsafe fn realloc(&self, ptr: *mut u8, old_layout: Layout, new_size: usize) -> *mut u8 {
        (&self).realloc(ptr, old_layout, new_size)
    }
}

/// Implements the trait methods required to make `ref RodalAlloc` the global allocator.
///
/// `ref RodalAlloc` is not announced as the global allocator,
/// instead it backs `RodalAlloc` as the global allocator
///
/// Result: `dealloc` and `realloc` are modified to only work on non-rodal-bound objects
unsafe impl<'a> GlobalAlloc for &'a RodalAlloc {
    #[inline]
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        self.get_sys().alloc(layout)
    }

    #[inline]
    unsafe fn alloc_zeroed(&self, layout: Layout) -> *mut u8 {
        self.get_sys().alloc_zeroed(layout)
    }

    #[inline]
    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        if !is_rodal_dump(ptr) {
            self.get_sys().dealloc(ptr, layout);
        }
    }

    #[inline]
    unsafe fn realloc(&self, ptr: *mut u8, old_layout: Layout, new_size: usize) -> *mut u8 {
        if is_rodal_dump(ptr) {
            if old_layout.size() >= new_size {
                ptr // Allocated area is large enough
            } else {
                // Have to copy to a new (really allocated) area
                let new_ptr = self.alloc(Layout::from_size_align_unchecked(
                    new_size,
                    old_layout.align(),
                ));
                let size = std::cmp::min(old_layout.size(), new_size);
                std::ptr::copy_nonoverlapping(ptr, new_ptr, size);
                new_ptr
            }
        } else {
            self.get_sys().realloc(ptr, old_layout, new_size)
        }
    }
}
